export { default as Article } from './article/Article'
export { default as DigitalPresence } from './digitalPresence/DigitalPresence'
export { default as Duty } from './duty/Duty'
export { default as Navbar } from './navbar/Navbar'
export { default as PageHeader } from './pageHeader/PageHeader'
export { default as SectionTitle } from './sectionTitle/SectionTitle'
export { default as PageTitle } from './pageTitle/PageTitle'
export { default as Services } from './services/Services'
export { default as ServiceBox } from './serviceBox/ServiceBox'
export { default as Team } from './team/Team'
export { default as Technique } from './technique/Technique'
export { default as Values } from './values/Values'


